db.rooms.insertOne({
	name: "Couple",
	accommodates: 2,
	price: 1,499,
	description: "These rooms are assigned to one person or a couple",
	roomsAvailable: 5,
	isAvailable: false
})

db.rooms.insertMany([
	{
		name: "Double",
		accommodates: 3,
		price: 2,999,
		description: "Double rooms are assigned to two people. Can also one double bed, or two twin beds",
		roomsAvailable: 10,
		isAvailable: false
	},
	{
		name: "Family",
		accommodates: 4,
		price: 4,999,
		description: "The room is larger than the standard room and will accommodate 4- 6 people, usually with an extra twin bed or sleeper sofa",
		roomsAvailable: 5,
		isAvailable: false
	}
])

db.rooms.insertMany([
	{
		name: "Queen",
		accommodates: 3,
		price: 5,999,
		description: "Standard Queen rooms are medium sized rooms and offer a queen sized bed",
		roomsAvailable: 10,
		isAvailable: false
	}.
	{
		name: "King",
		accommodates: 4,
		price: 7,999,
		description: "Standard King rooms are medium sized rooms and offer a king sized bed",
		roomsAvailable: 5,
		isAvailable: false
	}
])

db.rooms.find({name: "King"})

db.rooms.updateOne(
	{name: "Queen"},
	{
		$set: {
			roomsAvailable: 0,
		}

	}
)

db.rooms.deleteMany({
	roomsAvailable: 0
})
